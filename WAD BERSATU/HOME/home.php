<html>
<head>
	<title>Home</title>
	<style type="text/css">
		* {
			margin: 0;
			padding: 0;
		}

		.ContainerAtas {
			width: 100%;
			height: 650px;
			background-image: url(newyork.png);
			background-repeat: no-repeat;
			background-size: 100% 100%;
		}

		.Navbar {
			text-decoration: none;
			color: white;
			font-size: 24px;
		}
		.Navbar:hover {
			text-decoration: none;
			color: #c33e3e;
			font-size: 24px;
		}
		.NavbarTerpilih {
			text-decoration: none;
			color: #c33e3e;
			font-size: 24px;
		}
		.ContainerBawah {
			width: 100%;
			height: 500px;
			background-image: url(newyork2.png);
			background-repeat: no-repeat;
			background-size: 100% 100%;
		}
	</style>
</head>
<body>
	<div class = "ContainerAtas">
		<div style="float:right; margin-top: 30px;">
			<div style="float:left; margin-right: 30px;"> <a href="#" class = "NavbarTerpilih" > HOME </a> </div>
			<div style="float:left; margin-right: 30px;"> <a href="#" class = "Navbar" > CARA KERJA </a> </div>
			<div style="float:left; margin-right: 30px;"> <a href="#" class = "Navbar" > LAYANAN </a> </div>
			<div style="float:left; margin-right: 30px;"> <a href="#" class = "Navbar" > KONTAK </a> </div>
			<div style="float:left; margin-right: 50px;"> <a href="..\LOGIN\Login.php" class = "Navbar" > LOGIN </a> </div>
		</div>
		<center>
			<div>
				<div style="padding-top: 130px;"> <img src="logo.png"> </div>
				<div style="padding-top: 40px;"> <label style="color: white; font-size: 60px;"> MOTOMOTIF </label> </div>
			</div>
		</center>
		<div>
			<div style ="float:left; width: 350px; margin-left: 270px; margin-top: 50px;">
				<center> <div> <img src="image1.png"> </div> 
				<div> <label style="color: white; font-size: 28px;"> <b> Servis Jadi Lebih Mudah </b> </label> </div>
				<div> <label style="color: white; font-size: 25px;"> Pesan sekarang dan mekanik kami akan datang kemanapun </label> </div> </center>
			</div>
			<div style ="float: right; width: 350px; margin-right: 270px; margin-top: 50px;">
				<center> <div> <img src="image2.png"> </div>
				<div> <label style="color: white; font-size: 28px;"> <b> Berkualitas & Bergaransi </b> </label> </div>
				<div> <label style="color: white; font-size: 25px;"> Semua mekanik adalah mekanik adalah mekanik yang terbaik </label> </div>  </center>
			</div>
		</div>
	</div>
	<div class="ContainerBody">
		<center> <div style="margin-top: 20px;margin-bottom: 20px;"> <label style="font-size: 40px;"> FITUR - FITUR </label> </div> </center>

		<div>
			<div style = "float:left;width: 32%;">
				<center> <div> <img src="booking.png" style="width: 200px;"> </div>
				<div> <label> <b> Booking Services </b> </label> </div>
				<div> <label> Hanya dengan memesan melalui "Motomotif" maka konsumen bisa langsung melakukan service tanpa mengantri </label> </div> </center>
			</div>
			<div style = "float:right;width: 32%;">
				<center> <div> <img src="montir.png" style="width: 200px;"> </div>
				<div> <label> <b> E-Montir </b> </label> </div>
				<div> <label> Panggil montir untuk berbagai jenis layanan seperti ganti oli, cuci motor atau mobil dan lain lain </label> </div> </center>
			</div>
			<div style = "display:inline-block;width: 32%;">
				<center> <div> <img src="lokasi.png" style="width: 200px;"> </div>
				<div> <label> <b> Find a Service Places </b> </label> </div>
				<div> <label> Lacak keberadaan service terdekat </label> </div>
			</div> </center>
		</div>

		<div style="margin-top: 100px;"> 
			<div>
			<div style = "float:left;width: 32%;">
				<center> <div> <img src="artikel.png" style="width: 200px;"> </div>
				<div> <label> <b> Artikel </b> </label> </div>
				<div> <label> Lihat artikel mengenai tren dan topik terbaru seputar dunia motor dan mobil </label> </div> </center>
			</div>
			<div style = "float:right;width: 32%;">
				<center> <div> <img src="diskusi.png" style="width: 200px;"> </div>
				<div> <label> <b> Forum Diskusi </b> </label> </div>
				<div> <label> Melakukan diskusi mengenai dunia otomotif khususnya mengenai motor dan mobil </label> </div> </center>
			</div>
			<div style = "display:inline-block;width: 32%;">
				<center> <div> <img src="aksesoris.png" style="width: 200px;"> </div>
				<div> <label> <b> Aksesoris </b> </label> </div>
				<div> <label> Melihat list dan melakukan pembelian aksesoris kendaraan secara online </label> </div>
			</div> </center>
		</div>
			
		</div>
	</div>

	<div class="ContainerBawah">
		<div style = "float: left; color: white; margin-left: 170px; margin-top: 80px;">

			<center> <div style="margin-top: 100px;margin-bottom: 20px;"> <label style="font-size: 20px;"> <b> Ada yang bisa kami bantu? </b> </label> </div>
			<div style="margin-top: 20px"> <label> Customer service kami siap melayani anda setiap hari 7 hari seminggu </label> </div> 
			<div style="margin-top: 20px"> <label> Setiap hari buka pukul 7:00 s/d 24:00 </label> </div> 
			<div style="margin-top: 20px"> <label> (021) 2222 2502 </label> </div> </center>
		</div>

		<div style="float: right; color: white; margin-right: 200px; margin-top: 50px;">
			
			<center> <div style="margin-top: 40px; margin-bottom: 20px;"> <img src="logo.png" style="width: 150px;"> </div>
			<div style="margin-top: 10px"> <label> Melayani: </label> </div>
			<div style="margin-top: 10px"> <label> Honda. Yamaha. Suzuki. KTM. Kawasaki. </label> </div>
			<div style="margin-top: 10px"> <label> Toyota. Suzuki. Daihatsu. Mitsubishi. KIA. </label> </div> </center>
		</div>
		</div>

		<center> <div style="width: 100%; background-color: #202020;"> <label style="color: white;"> © Copyright 2019 Company Name. All Rights Reserved. | Privacy Policy </label></div>
</body>
</html>