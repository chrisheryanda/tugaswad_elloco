<?php

	include("koneksi.php");
	
	$sql = "SELECT * FROM register_table";
	$result = mysqli_query($conn, $sql);

	$row = mysqli_fetch_assoc($result)

?>

<html>
<head>
	<title>Home</title>
	<style type="text/css">
		* {
			margin: 0;
			padding: 0;
		}

		.ContainerAtas {
			width: 100%;
			height: 250px;
			background-image: url(newyork.png);
			background-repeat: no-repeat;
			background-size: 100% 100%;
		}

		.Navbar {
			text-decoration: none;
			color: white;
			font-size: 24px;
		}
		.Navbar:hover {
			text-decoration: none;
			color: #c33e3e;
			font-size: 24px;
		}
		.NavbarTerpilih {
			text-decoration: none;
			color: #c33e3e;
			font-size: 24px;
		}
		.ContainerBawah {
			width: 100%;
			height: 500px;
			background-image: url(newyork2.png);
			background-repeat: no-repeat;
			background-size: 100% 100%;
		}
		#ContainerLeft {
        	overflow:hidden;
        	height:100%;
        	width:39%;
 
    	}
    	#ContainerRight {
        	width:60%;
        	position:absolute;
        	margin-left:60%;
        }
	</style>
</head>
<body>
	<div style="float:left; margin-right: 50px;"> <a href="#" class = "Navbar" > Hi, <?= $row['Nama'] ?> 
	</a> </div>
	<div class = "ContainerAtas">
		<div style="float:right; margin-top: 30px;">
			<div style="float:left; margin-right: 30px;"> <a href="#" class = "NavbarTerpilih" > HOME </a> </div>
			<div style="float:left; margin-right: 30px;"> <a href="#" class = "Navbar" > CARA KERJA </a> </div>
			<div style="float:left; margin-right: 30px;"> <a href="#" class = "Navbar" > LAYANAN </a> </div>
			<div style="float:left; margin-right: 30px;"> <a href="#" class = "Navbar" > KONTAK </a> </div>
		</div>
		</div>
	</div>
	<div class="ContainerBody" style="height: 1000px">
		<div id="ContainerLeft"><CENTER><label style="font-size: 40px;"> ARTIKEL </label></center>
		<div style="margin-left: 15px; margin-top: 20px; float:left"><img src="Artikel 1.jpg">
		<br>
		<br>
		<br>
		<img src="Artikel 2.jpg">
		<br>
		<br>
		<br>
		<img src="Artikel 3.jpg">
		<br>
		<br>
		<br>
		<img src="Artikel 4.jpg">
		<br>
		<br>
		<br>
		<img src="Artikel 5.jpg">
	</div>
		<div style="margin-left: 0px; margin-top: 50px; float:right">
			<a href="https://www.liputan6.com/tag/mudik-lebaran-2019">Jelang Mudik Lebaran</a>
			<br>
			<a href="https://www.liputan6.com/tag/mudik-lebaran-2019">Toyota Pangkas Harga Suku Cadang</a>
		</div>
		<div style="margin-top: 130px; float:right; margin-right: 9px">
			<a href="https://www.idntimes.com/automotive/car/iip-afifullah/6-tips-sederhana-hemat-bbm-mobil-biar-gak-boros-pengeluaran-exp-c1c2">Ternyata Salah Kaprah, Abaikan 6</a>
			<br>
			<a href="https://www.idntimes.com/automotive/car/iip-afifullah/6-tips-sederhana-hemat-bbm-mobil-biar-gak-boros-pengeluaran-exp-c1c2">Cara Hemat BBM Mobil Berikut</a>
		</div>
		<div style="margin-top: 130px; float:right">
			<a href="https://www.otosia.com/berita/yamaha-r15-punya-3-warna-baru-menggoda-harga-tetap.html">Yamaha R15 Punya 3 Warna Baru</a>
			<br>
			<a href="https://www.otosia.com/berita/yamaha-r15-punya-3-warna-baru-menggoda-harga-tetap.html">Harga Tetap</a>
		</div>
		<div style="margin-top: 130px; float:right; margin-right: 50px">
			<a href="https://www.100kpj.com/mobil/2239-5-mobil-pilihan-cocok-buat-drifting-harga-di-bawah-rp80-juta">Mobil Harian Bisa Dipakai</a>
			<br>
			<a href="https://www.100kpj.com/mobil/2239-5-mobil-pilihan-cocok-buat-drifting-harga-di-bawah-rp80-juta">Drifting, Asal...</a>
		</div>
		<div style="margin-top: 130px; float:right">
			<a href="https://otomotif.tempo.co/read/1184460/melongok-kemewahan-toyota-land-cruiser-prado-2019">Harga Land Cruiser Prado, Review,</a>
			<br>
			<a href="https://otomotif.tempo.co/read/1184460/melongok-kemewahan-toyota-land-cruiser-prado-2019">Spesifikasi dan Kredit April 2019</a>
		</div>	
		<div id="ContainerRight">
			<img src="Right.jpg">
			<br>
			<br>
			<div style="float: left"><label>Motor Seharga Mobil ini Nyaris Ludes di</label>
				<br>
				<label>IIMS 2019</label>
				<br><br><br>
				<label>Kasian, ini Deretan Mobil Enggak Laku-</label>
				<br>
				<label>laku di IIMS Gan!</label>
				<br><br><br>
				<label>Lambretta Punya Fitur Jadul Anti-Maling</label>
				<br>
				<label>Motor, Simak nih Gan!</label>
				<br><br><br>
				<label>Mulai Dari Irit Bensin Sampai Ngurangin</label>
				<br>
				<label>Polusi, ini Cara Eco Driving</label>
				<br><br><br>
				<label>Suzuki India Resmi Perkenalkan GSX-S750</label>
				<br>
				<label>Terbaru, Tampil Lebih Gahar</label></div>
		</div>	
	</div>

	<div class="ContainerBawah">
		<div style = "float: left; color: white; margin-left: 170px; margin-top: 80px;">

			<center> <div style="margin-top: 100px;margin-bottom: 20px;"> <label style="font-size: 20px;"> <b> Ada yang bisa kami bantu? </b> </label> </div>
			<div style="margin-top: 20px"> <label> Customer service kami siap melayani anda setiap hari 7 hari seminggu </label> </div> 
			<div style="margin-top: 20px"> <label> Setiap hari buka pukul 7:00 s/d 24:00 </label> </div> 
			<div style="margin-top: 20px"> <label> (021) 2222 2502 </label> </div> </center>
		</div>

		<div style="float: right; color: white; margin-right: 200px; margin-top: 50px;">
			
			<center> <div style="margin-top: 40px; margin-bottom: 20px;"> <img src="logo.png" style="width: 150px;"> </div>
			<div style="margin-top: 10px"> <label> Melayani: </label> </div>
			<div style="margin-top: 10px"> <label> Honda. Yamaha. Suzuki. KTM. Kawasaki. </label> </div>
			<div style="margin-top: 10px"> <label> Toyota. Suzuki. Daihatsu. Mitsubishi. KIA. </label> </div> </center>
		</div>
		</div>

		<center> <div style="width: 100%; background-color: #202020;"> <label style="color: white;"> © Copyright 2019 Company Name. All Rights Reserved. | Privacy Policy </label></div>
</body>
</html>